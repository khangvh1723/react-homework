import { combineReducers } from "redux";
import { BTDatVeReducer } from "./BTDatVe/slice";
export const rootReducer = combineReducers({
    BTDatVe: BTDatVeReducer,
})