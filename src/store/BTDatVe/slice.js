import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    chairBookings: [],
    chairBookeds: [],
}

const BTDatVeSlice = createSlice({
    name: 'BTDatVe',
    // object literal
    initialState,
    reducers: {
        setChairBookings: (state, action) => {
            console.log("🚀 ~ file: slice.js:18 ~ action:", action)
            // 
            const index = state.chairBookings.findIndex((e) => e.soGhe === action.payload.soGhe)
            if (index !== -1) {
                state.chairBookings.splice(index, 1)
            } else {
                state.chairBookings.push(action.payload)
            }

        },
        setChairBookeds: (state, { payload }) => {
            console.log("🚀 ~ file: slice.js:30 ~ payload:", payload)
            // clone state.chairBookings and clone state.chairBooked to state.chairBooked
            state.chairBookeds = [...state.chairBookeds, ...state.chairBookings]
            // set state.chairBookings to empty array
            state.chairBookings = []
        }
    }
})

export const { actions: BTDatVeActions, reducer: BTDatVeReducer } = BTDatVeSlice