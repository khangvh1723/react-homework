import React from 'react'
import Chair from './Chair'

const ChairList = ({ data }) => {
    return (
        <div>
            {
                data.map(hangGhe => {
                    return (
                        <div className="d-flex mt-3 justify-content-center" key={hangGhe.hang}>
                            <p className='me-3' style={{ width: '30px' }}>{hangGhe.hang}</p>
                            <div className='d-flex' style={{ gap: '16px' }}>
                                {
                                    hangGhe.danhSachGhe.map(ghe => {
                                        return <Chair ghe={ghe} key={ghe.soGhe} />
                                    })
                                }
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default ChairList