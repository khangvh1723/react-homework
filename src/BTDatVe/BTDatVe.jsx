import React from 'react'
import './styles.scss'
import data from './data.json'
import ChairList from './ChairList'
import Result from './Result'

const BTDatVe = () => {
    return (
        <div className='bookingMovie'>
            <div className="row">
                <div className="col-lg-8">
                    <h1 className='text-center'>ĐẶT VÉ XEM PHIM CYBERLEARN.VN</h1>
                    <div className="Screen">
                        <p className='text-center'>Màn hình</p>
                        <div className="screen-visual"></div>
                        {/* Chair list */}
                        <ChairList data={data} />
                    </div>
                </div>
                <div className="col-lg-4">
                    {/* Ticket book result */}
                    <Result />
                </div>
            </div>

        </div>
    )
}

export default BTDatVe